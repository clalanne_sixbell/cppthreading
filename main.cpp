#include <iostream>
#include <future>
#include <chrono>
#include <thread>

/*task, because is passed to std::async*/
int startDTMF() {
    std::cout << "[startDTMF]\n";
    std::chrono::seconds timespan(1);

    std::this_thread::sleep_for(timespan);

    std::cout << "[startDTMF] end\n";
    return 0;
}

int main() {
    std::cout << "hello world!" << std::endl;

    auto fut = std::async(std::launch::async, startDTMF);
    //auto fut = std::async(startDTMF);

    std::cout << "bye world!" << std::endl;
    auto res{fut.get()};
    std::cout << "ending async work: " << res <<std::endl;
    return 0;
}
